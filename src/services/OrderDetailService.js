export default (Axios, baseUrl) => {
    return {
        getAll() {
            return Axios.get(`${baseUrl}/OrderDetails`);
        },
        getProduct(id) {
            return Axios.get(`${baseUrl}/OrderDetails/${id}`);
        },
        create(params) {
            return Axios.post(`${baseUrl}/OrderDetails`, params);
        },
        update(id, params) {
            return Axios.put(`${baseUrl}/OrderDetails/${id}`, params);
        },
        remove(id) {
            return Axios.delete(`${baseUrl}/OrderDetails/${id}`);
        }
    }
}