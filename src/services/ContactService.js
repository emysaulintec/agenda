export default (Axios, baseUrl) => {
    return {
        getAll(userId) {
            return Axios.get(`${baseUrl}/contact/by-users/${userId}`);
        },
        getContact(id) {
            return Axios.get(`${baseUrl}/contact/${id}`);
        },
        create(params) {
            return Axios.post(`${baseUrl}/contact`, params);
        },
        update(id, params) {
            return Axios.put(`${baseUrl}/contact/${id}`, params);
        },
        remove(id) {
            return Axios.delete(`${baseUrl}/contact/${id}`);
        }
    }
}