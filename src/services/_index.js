import Axios from 'axios';
import ContactService from './ContactService';
import PhoneService from './PhoneService';
import UserService from './UserService';
import ProductService from './ProductService';
import OrderService from './OrderService';
import TableService from './TableService';
import OrderDetailService from './OrderDetailService';

Axios.defaults.headers.common.Accept = "application/json";

export default {
    contactService: new ContactService(Axios, window.baseUrl),
    phoneService: new PhoneService(Axios, window.baseUrl),
    userService: new UserService(Axios, window.baseUrl),
    productService: new ProductService(Axios, window.baseUrl),
    orderService: new OrderService(Axios, window.baseUrl),
    tableService: new TableService(Axios, window.baseUrl),
    orderDetailService: new OrderDetailService(Axios, window.baseUrl),
}