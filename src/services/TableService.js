export default (Axios, baseUrl) => {
    return {
        getAll() {
            return Axios.get(`${baseUrl}/Table`);
        },
        getProduct(id) {
            return Axios.get(`${baseUrl}/Table/${id}`);
        },
        create(params) {
            return Axios.post(`${baseUrl}/Table`, params);
        },
        update(id, params) {
            return Axios.put(`${baseUrl}/Table/${id}`, params);
        },
        remove(id) {
            return Axios.delete(`${baseUrl}/Table/${id}`);
        }
    }
}