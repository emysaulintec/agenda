export default (Axios, baseUrl) => {
    return {
        getAll() {
            return Axios.get(`${baseUrl}/Order`);
        },
        getAllInvoices() {
            return Axios.get(`${baseUrl}/Order/pending`);
        },
        getProduct(id) {
            return Axios.get(`${baseUrl}/Order/${id}`);
        },
        create(params) {
            return Axios.post(`${baseUrl}/Order`, params);
        },
        update(id, params) {
            return Axios.put(`${baseUrl}/Order/${id}`, params);
        },
        remove(id) {
            return Axios.delete(`${baseUrl}/Order/${id}`);
        },
        generateOneInvoice(id) {
            return Axios.get(`${baseUrl}/Order/onePayment/${id}`);
        },
        generateSplitInvoice(id) {
            return Axios.get(`${baseUrl}/Order/splitPayment/${id}`);
        },
        getOrderByTableId(tableId) {
            return Axios.get(`${baseUrl}/Order/Table/${tableId}`);
        },
        toPay(id) {
            return Axios.put(`${baseUrl}/Order/pay/${id}`);
        }
    }
}