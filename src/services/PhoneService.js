export default (Axios, baseUrl) => {
    return {
        getAll() {
            return Axios.get(`${baseUrl}/phone`);
        },
        getContact(id) {
            return Axios.get(`${baseUrl}/phone/${id}`);
        },
        create(params) {
            return Axios.post(`${baseUrl}/phone`, params);
        },
        update(id, params) {
            return Axios.put(`${baseUrl}/phone/${id}`, params);
        },
        remove(id) {
            return Axios.delete(`${baseUrl}/phone/${id}`);
        }
    }
}