export default (Axios, baseUrl) => {
    return {
        getAll() {
            return Axios.get(`${baseUrl}/Product`);
        },
        getProduct(id) {
            return Axios.get(`${baseUrl}/Product/${id}`);
        },
        create(params) {
            return Axios.post(`${baseUrl}/Product`, params);
        },
        update(id, params) {
            return Axios.put(`${baseUrl}/Product/${id}`, params);
        },
        remove(id) {
            return Axios.delete(`${baseUrl}/Product/${id}`);
        }
    }
}