export default (Axios, baseUrl) => {
    return {
        login(params) {
            return Axios.post(`${baseUrl}/User/login`, params);
        },
        prepare(token) {
            Axios.defaults.headers.common.Authentication = token;
        }
    }
}