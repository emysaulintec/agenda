import { DialogProgrammatic as Dialog } from 'buefy';
import { ToastProgrammatic as Toast } from 'buefy';


import UBuefyService from './UBuefyService';

export default {
    uBuefyService: new UBuefyService(Dialog, Toast)
}