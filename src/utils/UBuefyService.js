export default (dialog, Toast) => {
    return {
        confirmCustomDelete(itemToDelete, callback) {
            dialog.confirm({
                title: "Borrar",
                message: `Estas seguro que quieres <b>borrar</b> este ${itemToDelete}? Esta acción no puede revertirse..`,
                confirmText: `Borrar ${itemToDelete}`,
                type: "is-danger",
                hasIcon: true,
                onConfirm: () => {
                    if (callback) callback();
                    Toast.open(`${itemToDelete} borrado!`);
                },
            });
        }
    }
}