import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import VueTelInput from 'vue-tel-input'


import App from './App.vue'
import router from './router'
import services from './services/_index.js'
import utils from './utils/_index.js'


Vue.config.productionTip = false


Vue.use(Vuex)
Vue.use(VueTelInput)
Vue.use(Buefy)


const store = new Vuex.Store({
    state: {
        user: {
            user: {},
            token: null
        }
    }
});

Vue.use(VueAxios, axios)



Vue.use({
    install(Vue) {
        Object.defineProperty(Vue.prototype, '$utils', {
            value: utils
        })
    }
})


Vue.use({
    install(Vue) {
        Object.defineProperty(Vue.prototype, '$services', {
            value: services
        })
    }
})

router.beforeEach((to, from, next) => {
    if (!store.state.user.token && to.path != '/login')
        next('/login')
    next();
});


new Vue({
    router,
    store,
    Buefy,
    render: h => h(App)
}).$mount('#app')