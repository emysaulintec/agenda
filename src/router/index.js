import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Product from '../views/Product.vue'
import Orders from '../views/Orders.vue'
import CreateOrUpdateProduct from '../views/CreateOrUpdateProduct.vue'
import CreateOrUpdateOrder from '../views/CreateOrUpdateOrder.vue'
import Login from '../views/Login.vue'
import Invoices from '../views/Invoices'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/product',
        name: 'Product',
        component: Product
    },
    {
        path: '/product/create',
        name: 'Create',
        component: CreateOrUpdateProduct
    },
    {
        path: '/product/:id/update',
        name: 'Update',
        component: CreateOrUpdateProduct
    },
    {
        path: '/order',
        name: 'Order',
        component: Orders
    },
    {
        path: '/invoices',
        name: 'Invoice',
        component: Invoices
    },
    {
        path: '/order/create',
        name: 'Order',
        component: CreateOrUpdateOrder
    },
    {
        path: '/order/:id/update',
        name: 'UpdateOrder',
        component: CreateOrUpdateOrder
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    }
]

const router = new VueRouter({
    routes
})

export default router